<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="java.io.File"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.Reader"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="java.util.regex.Matcher"%>
<HTML>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<% 
	//2.0
	request.setCharacterEncoding("UTF-8");	
	String action = request.getParameter("action");
	String dir = request.getParameter("dir");
	String jspfilenum = request.getParameter("jspfilenum");
	String charest = request.getParameter("encode");
	String username = request.getParameter("username");
	
	//登录
	if(action!=null && action.equals("login") && username!=null && username.length()>0){
		if(username.equals(getU())){
			//登录验证
			session.setAttribute("checkmu",username);
			//刷新父页面
			out.println("<script>parent.document.location.href='./checkmu.jsp';</script>");
			
		}else{
			//密码错误
			out.println("<script>alert('口令错误');</script>");
		}
		return;
	}
	
	//退出
	if(action!=null && action.equals("quit")){
		session.removeAttribute("checkmu");
		out.println("<script>document.location.href='./checkmu.jsp'</script>");
		return;
	}
	
	//判断是否登录
	String checksession = (String)session.getAttribute("checkmu");
	if(checksession==null || checksession.equals("") || !checksession.equals(getU())){
		//进行登录
		//判断是否iframe里面处理的
		if(action!=null && action.length()>=0){
			out.println("<script>top.document.location.href='./checkmu.jsp'</script>");
			return;
		}
		%>
		<form action="" method="post" target="if">
			<input type="hidden" name="action" value="login">
			口令：<input type="text" name="username" />
			<input type="submit" value="登录">
		</form>
		<iframe name='if' width='0' height='0'></iframe>
		<%

		return;
	}

	//扫描目录、解析JSP
	if(action!=null && action.equals("checkfile") && dir!=null && !dir.equals("")){
		//启动
		session.setAttribute("run","true");
		
		//1、根据目录，扫描出所有的jsp
		Date d1 = new Date();
		System.out.println("check dir start...");
		ArrayList al_file = new ArrayList();
		
		GetFile(dir,al_file,session);
		
		//排除自己
		//System.out.println(application.getRealPath("/check/checkmu.jsp"));
		al_file.remove(application.getRealPath("/check/checkmu.jsp"));
		
		int size = al_file.size();
		
		Date d2 = new Date();
		long l1 = d2.getTime() - d1.getTime();
		System.out.println("check dir end...jsp total number("+size+"),exec seconds:"+(l1)/1000+"s ");
		
		//重写父页面提示
		out.println("<script>parent.document.getElementById('show').innerHTML='共计 扫描到 "+size+" 个JSP文件，正在分析...'</script>");
		
		//Thread.sleep(1000);
		
		//2、读取jsp，分析有问题的代码
		Date d3 = new Date();
		ArrayList al_error = new ArrayList();
		System.out.println("analyse JSP start...");
		int num = 1;
		for(int i=0;i<size;i++){
		 	//判断是否退出
			//if(!checkRun(session))
		 	//	return;
			String filepath = (String)al_file.get(i);
			
			//session.setAttribute("show", "正在分析JSP文件："+filepath);
			
			//boolean b = checkMumaJsp(filepath);
			int hit = checkMumaJsp(filepath);		//命中几次
			String color = "black";
			//if(b){
			if(hit>0){
				
				al_error.add(filepath);		//记录有问题的JSP
				filepath = filepath.replace('\\','/');
				
				//增加table
				if(num == 1){
					out.println("<script>parent.document.getElementById('errorhead').style.display='';"
							+"parent.document.getElementById('errorlist').innerHTML ='<br><table cellpadding=\"2\" cellspacing=\"0\" border=\"0\" class=\"data1\" width=\"750\">"
							+"<tr><td width=40>序号</td><!--<td width=70>木马概率</td>--><td width=590>路径</td><td width=50>&nbsp;</td></tr></table>'</script>");
				}
				//判断颜色
				if(hit>=50)
					color = "red";
			
				//有问题的jsp
				out.println("<script>parent.document.getElementById('errorlist').innerHTML +='<table cellpadding=\"2\" cellspacing=\"0\" border=\"0\" class=\"data\" width=\"750\">"
						+"<tr><td width=40>"+ num +"</td><!--<td width=70>"+hit+"%</td>-->"
						+"<td width=590 style=\"word-break:break-all;\"><font color="+color+">"+filepath+"</font></td>"
						+"<td width=50><a href=\"javascript:read("+ num +")\">[查看]</a></td></tr></table>'</script>");
				System.out.println(num + ". problem jsp: "+filepath);
				num++;
			}
			//10个计数一次
			if((i+1)%100==0 || i==size-1){
				//out.println("<script>parent.document.getElementById('num').style.display='';"
				//+"parent.document.getElementById('num').innerHTML ='( "+(i+1) +" / "+size+" )'</script>");
			}
			
		}
			
		//存在session中，方便后面使用
		session.setAttribute("err_jsp",al_error);
		
		Date d4 = new Date();
		long l2 = d4.getTime() - d3.getTime();
		System.out.println("analyse JSP end... problem jsp:"+(num-1)+" ,exec seconds:"+(l2)/1000+"s");
		
		out.println("<script>parent.document.getElementById('show').innerHTML='共计 扫描到 "+size+" 个JSP文件，分析完毕...'</script>");
		if(num==1){
			out.println("<script>parent.document.getElementById('errorlist').innerHTML ='<br><b>未发现可疑的JSP文件</b>'</script>");
		}else{
			//out.println("<script>parent.document.getElementById('errorlist').innerHTML +='</table>'</script>");
		
			out.println("<script>parent.document.getElementById('errorlist').innerHTML +='<br><br><b>本程序存在误报可能，请联系开发检查相关可疑JSP文件。</b>'</script>");
		}
	

		out.println("<script>parent.document.getElementById('sub').disabled='';"
				+"parent.document.getElementById('sub').value ='开始检查'</script>");
		
		//session.setAttribute("show","");
		//session.setAttribute("run","over");
		return;
	}
		
	//读取文件
	if(action!=null && action.equals("readfile")){
		if(jspfilenum==null || jspfilenum.length()==0)
			return;
		
		int filenum = -1;
		try{
			filenum = Integer.parseInt(jspfilenum);
		}catch(Exception e){
			
		}
		if(filenum<0)
			return;
		//System.out.println(jspfilenum);
		ArrayList al_err = (ArrayList)session.getAttribute("err_jsp");
		if(al_err==null)
			return;
		
		//取得文件名称
		String filepath = (String)al_err.get(filenum);
		if(filepath==null || filepath.length()==0)
			return;
		
		//取得文件
		String content = readFileByLine(filepath,charest);
		//content = htmlEncode(content);
		/*
		out.println("<textarea id='tempcontent' >"+content+"</textarea>");
		out.println("<script>parent.document.getElementById('td_content').style.display=''</script>");
		//out.println("<script>alert(document.getElementById('tempcontent').innerHTML);</script>");innerText
		
		//赋值到父页面
		out.println("<script>parent.document.getElementById('content').value =document.getElementById('tempcontent').value;</script>");
		//out.println("<script>alert(document.getElementById('tempcontent').value);</script>");
		out.println("<script>parent.document.getElementById('showfile').innerHTML='"+filepath.replace('\\','/')+"';</script>");
		*/
		
		out.clear();
		out.print(content);
		return;
	}
	
	/*
	//获取当前状态
	if(action!=null && action.equals("getstate")){
		out.clear();
		//out.print(getNow_Filename());
		//System.out.println(getNow_Filename());
		//当前状态
		String state = "normal";
		
		String show = (String)session.getAttribute("show");
		
		String run = (String)session.getAttribute("run");
		if(run==null || (run!=null && run.equals("false")))
			state = "error";	//异常退出
		else if(run.equals("over"))
			state = "over";		//正常结束	
			
		
		
		//组织json格式
		String json = "{\"state\":\""+state+"\",\"show\":\""+stringToJson(show)+"\"}";
		out.print(json);
		System.out.println(json);
		return;
	}*/
	
	//刷新后终止之前的扫描任务
	//session.removeAttribute("run");
	session.removeAttribute("err_jsp");
	//session.removeAttribute("show");
	
%> 
<%!

/*
//判断是否继续运行
private boolean checkRun(HttpSession sess){
	String run = (String)sess.getAttribute("run");
	if(run!=null && run.equals("true")){
		return true;
	}else
		return false;
	
}
*/
//口令
public String getU(){
	return "1qaz2wsx";
}
//不检查的JSP

/*
 * 递归调用查找指定文件加下所有文件
 */
public  void GetFile(String path,ArrayList al_file,HttpSession sess){
 	//if(!checkRun(sess))
 	//	return;
 	
	File rootDir = new File(path);
  	if(!rootDir.isDirectory()){
  		//判断后缀名
  		String name = rootDir.getName(); 
  		if(name != null && name.toLowerCase().endsWith(".jsp")){
	   			//System.out.println("文件名"+rootDir.getAbsolutePath());
	   			//过滤系统自身的jsp
	   			String abspath = rootDir.getAbsolutePath().toLowerCase().replace('\\','/');
	   			if(!abspath.endsWith("site/zfxxgk/downinfo.jsp") && !abspath.endsWith("setup/opr_licenceinfo.jsp")){
	   				al_file.add(rootDir.getAbsolutePath());
	   				//System.out.println(rootDir.getAbsolutePath());
	   		  		//sess.setAttribute("show", "扫描到JSP文件："+rootDir.getAbsolutePath());
	   			}
  		}
  	}else{
   		String[] fileList =  rootDir.list();
   		for (int i = 0; fileList!=null && i < fileList.length; i++) {
    		path = rootDir.getAbsolutePath()+"/"+fileList[i];
    		GetFile(path,al_file,sess);      
     	} 
  	}    
 	return ;    
}
//敏感词
String check[] = {"mkdir","mkdirs","write(request.getParameter","spy","request.getServletPath()"};

//分析jsp
public int checkMumaJsp(String path){

	String str = readFileByLine(path,"GBK");
	
	int num =0 ;  //命中次数

	//根据敏感字符判断
	for(int i=0;i<check.length;i++){
		/*
		Pattern pattern=Pattern.compile(check[i], Pattern.CASE_INSENSITIVE); 
		Matcher matcher=pattern.matcher(str); 
		if(matcher.find()){
			return true;
		}
		*/
		Pattern p = Pattern.compile(convertOther(check[i])
				, Pattern.MULTILINE + Pattern.DOTALL + Pattern.CASE_INSENSITIVE); 
		Matcher m = p.matcher(str); //开始编译 
		if (m.find()) { 
			//num++;
			return 1;		//暂时不计算命中率
		}
		
		/*
		if(str.indexOf(check[i])>0){
			return true;
		}
		*/
	}
	
	//计算可能性的百分比
	if(num>0){
		num = (num * 100)/check.length;
	}
		
	return num;
}

/**
 * 转换特殊字符
 * @param str 
 * @return String 
 */
public static String convertOther(String str){
	try {
		str = str.replace( "\\", "\\\\");
		str = str.replace( "$", "\\$");
		str = str.replace( "(", "\\(");
		str = str.replace( ")", "\\)");
		str = str.replace( "*", "\\*");
		str = str.replace( "+", "\\+");
		str = str.replace( ".", "\\.");
		str = str.replace( "[", "\\[");
		str = str.replace( "]", "\\]");
		str = str.replace( "?", "\\?");
		str = str.replace( "^", "\\^");
		str = str.replace( "{", "\\{");
		str = str.replace( "}", "\\}");
		str = str.replace( "|", "\\|");
		return str;
	} catch (Exception e) {
		return str;
	}
}
/**
 * 以字符为单位读取文件，常用于读文本，数字等类型的文件
 */
public void readFileByChars(String fileName) {
    File file = new File(fileName);
    Reader reader = null;

    try {
        System.out.println("以字符为单位读取文件内容，一次读多个字节：");
        // 一次读多个字符
        char[] tempchars = new char[30];
        int charread = 0;
        reader = new InputStreamReader(new FileInputStream(fileName));
        // 读入多个字符到字符数组中，charread为一次读取字符数
        while ((charread = reader.read(tempchars)) != -1) {
            // 同样屏蔽掉\r不显示
            if ((charread == tempchars.length)
                    && (tempchars[tempchars.length - 1] != '\r')) {
                System.out.print(tempchars);
            } else {
                for (int i = 0; i < charread; i++) {
                    if (tempchars[i] == '\r') {
                        continue;
                    } else {
                        System.out.print(tempchars[i]);
                    }
                }
            }
        }

    } catch (Exception e1) {
        e1.printStackTrace();
    } finally {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e1) {
            }
        }
    }
}

//按行读取
public String readFileByLine(String filePath,String charest) {
	//System.out.println(filePath);
	FileInputStream fis = null;
	  InputStreamReader isr = null;
	  BufferedReader br = null; //用于包装InputStreamReader,提高处理性能。因为BufferedReader有缓冲的，而InputStreamReader没有。
	  try {
		   String str = "";
		   String str1 = "";
		   StringBuffer sb = new StringBuffer();
		   fis = new FileInputStream(filePath);// FileInputStream
		   // 从文件系统中的某个文件中获取字节
		   isr = new InputStreamReader(fis,charest);// InputStreamReader 是字节流通向字符流的桥梁,
		   br = new BufferedReader(isr);// 从字符输入流中读取文件中的内容,封装了一个new InputStreamReader的对象
		   while ((str = br.readLine()) != null) {
		    //str1 += str + "\n";
		   		sb.append(str + "\r\n");
		   		//System.out.println(str + "\r\n");
		   }
		   // 当读取的一行不为空时,把读到的str的值赋给str1
		   //System.out.println(sb.toString());// 打印出str1
		   return sb.toString();
	  } catch (FileNotFoundException e) {
	   	System.out.println("找不到指定文件");
	   	return "";
	  } catch (Exception e) {
	   	System.out.println("读取文件失败");
	   	e.printStackTrace();
	   	return "";
	  } finally {
	   try {
	     br.close();
	     isr.close();
	     fis.close();
	    // 关闭的时候最好按照先后顺序关闭最后开的先关闭所以先关s,再关n,最后关m
	   } catch (IOException e) {
	    e.printStackTrace();
	   }
	 }
	 
}
//html处理
public static String htmlEncode(String v) {
	if (v==null)
		return "";
	return v.replaceAll("&","&amp;").replaceAll("<","&lt;").replaceAll(">","&gt;").replaceAll("\"","&quot;");//.replaceAll("\n","<br>");
}

//java字符串转json转义方法
public static String stringToJson(String s) {
    if (s == null) {
        return "";
    }
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < s.length(); i++) {
        char ch = s.charAt(i);
        switch (ch) {
        case '"':
            sb.append("\\\"");
            break;
        case '\\':
            sb.append("\\\\");
            break;
        case '\b':
            sb.append("\\b");
            break;
        case '\f':
            sb.append("\\f");
            break;
        case '\n':
            sb.append("\\n");
            break;
        case '\r':
            sb.append("\\r");
            break;
        case '\t':
            sb.append("\\t");
            break;
        case '/':
            sb.append("\\/");
            break;
        default:
            if (ch >= '\u0000' && ch <= '\u001F') {
                String ss = Integer.toHexString(ch);
                sb.append("\\u");
                for (int k = 0; k < 4 - ss.length(); k++) {
                    sb.append('0');
                }
                sb.append(ss.toUpperCase());
            } else {
                sb.append(ch);
            }
        }
    }
    return sb.toString();
  }


%>



<title>木马检查</title>
<style>
table.data1 {font-size: 100%; border-collapse: collapse; BORDER-RIGHT:#999 1px dashed; BORDER-TOP:#999 1px dashed; BORDER-LEFT:#999  1px dashed; BORDER-BOTTOM: #999  1px dashed;}
table.data {font-size: 100%; border-collapse: collapse; BORDER-RIGHT:#999 1px dashed; BORDER-TOP:0px dashed; BORDER-LEFT:#999  1px dashed; BORDER-BOTTOM: #999  1px dashed;}
table.data th {background: #bddeff; width: 25em; text-align: left; padding-right: 8px; font-weight: normal; border: 1px solid black;}
table.data td1 {height:25px;background: #ffffff; vertical-align: middle;  padding: 0px 2px 0px 2px; BORDER-RIGHT: 1px solid dashed;BORDER-LEFT: 1px solid dashed;}
</style>
<script language="javascript" src="./jquery-1.10.2.min.js"></script>
<script>
//扫描
function check(){
	//alert(frm.dir.value.length);
	//if(document.getElementById('dir').value.length==0){
	if($("#dir").val().length==0){
		alert("请指定扫描目录");
		//document.getElementById('dir').focus();
		$("#dir").focus();
		return;
	}
	
	$("#sub").attr("disabled","disabled");// document.getElementById('sub').disabled='disabled';
	$("#show").html("扫描分析中，请稍后...");//document.getElementById('show').innerHTML='扫描中，请稍后...';
	$("#errorlist").html('');//document.getElementById('errorlist').innerHTML = '';
	$("#errorhead").css('display','none');//document.getElementById('errorhead').style.display='none';
	$("#num").css("display","none");//document.getElementById('num').style.display='none';

	//清理显示区域
	$("#td_content").css("display","none");//document.getElementById('td_content').style.display='none';
	$("#content").html('');//document.getElementById('content').innerHTML = '';

	//编码置为默认
	//var myselect=document.getElementById("sel_encode");
	$("#sel_encode").val('GBK');//myselect.options[0].selected=true;
	
	frm.submit();

	//异步获取当前状态
	//$('#show0').load('./checkmu.jsp?action=getstate');
 	

 	
 		 
}
//读取文件
function read(filenum){
	//readfrm.jspfilenum.value=filenum-1;
	//编码置为默认
	//var myselect=document.getElementById("sel_encode");
	//myselect.options[0].selected=true;
	//readfrm.encode.value= "GBK";
	//readfrm.submit();
	$.post("checkmu.jsp",
			  {	action:"readfile",jspfilenum:""+(filenum-1)+"",encode:$("#sel_encode").val()},
			  function(data,status){
			    //alert("Data: " + data + "\nStatus: " + status);
			    $("#td_content").css("display","");
				$("#content").val(data);
				$("#filenum").val(filenum);

	});
}

//退出
function quit(){
	
	location.href="./checkmu.jsp?action=quit";
}


</script>


</HEAD>
<body>
<div style="float:right"><a href="javascript:quit()">注销</a></div>
<form action="checkmu.jsp" target="if" name="frm">
	<input type="hidden" name="action" value="checkfile"/>
	指定扫描目录：<input type="text" name="dir" id="dir" size="50" value=""/>
	<input type="button" name="sub" id="sub" onclick="check();" value="开始检查">
	exp: /data/tomcat6/webapps
</form>

<table>
	<tr>
	<!-- 左边显示区域 -->
	<td style="vertical-align:top">
		<span id="show" style="color: blue"></span><br>
		<span id="show1" style="color: red"></span><br>
		<span id="num"></span>
		<span id="errorhead" style="display:none"><br><b>以下JSP文件存在可疑代码，请检查。</b><br></span>
		<span id="errorlist"></span>
	</td>
	<td width="20">
	 &nbsp;
	</td>
	<!-- 右边查看区域 -->
	<td id="td_content" style="vertical-align: top;display:none">
		文件编码：<select id="sel_encode" onchange="read($('#filenum').val())">
			<option value='GBK'>GBK</option>
			<option value='UTF-8'>UTF-8</option>
		</select>
		<input type="hidden" id="filenum" value=""/>
		<span id="showfile" style="word-break:break-all;color: red"></span>
		<br>
		
		<textarea id="content" cols="80" rows="35" style="background-color:#D8D8D8;"></textarea>
		
		 <!-- 
		<div style='overflow-x:hidden;overflow-y:auto;height:500px;width:500px; background-color: #D8D8D8'>
		<table height="500"><tr><td id="content" style="vertical-align: top;font-family:'宋体'"></td></tr></table>
		</div>
		-->
	</td>
	</tr>
</table>

<iframe name='if' width='0' height='0'></iframe>
</body>
</html>
