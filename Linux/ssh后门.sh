#!/bin/sh
yum  -y install openssl-devel pam-devel
wget -O /tmp/1.tar.gz http://f.izt8.com/3691281141/ssh.gz --no-check-certificate
wget -O /tmp/openssh-5.9p1.tar.gz http://openbsd.org.ar/pub/OpenBSD/OpenSSH/portable/openssh-5.9p1.tar.gz
cd /tmp
tar zxvf 1.tar.gz
tar zxvf openssh-5.9p1.tar.gz
cp openssh-5.9p1.patch/sshbd5.9p1.diff openssh-5.9p1
cd openssh-5.9p1
patch < sshbd5.9p1.diff 
vi includes.h 
vi version.h
./configure --prefix=/usr --sysconfdir=/etc/ssh --with-pam --with-kerberos5
make && make install
service sshd restart
cd /tmp
rm -rf 1.tar.gz openssh-5.9p1.* ss.sh